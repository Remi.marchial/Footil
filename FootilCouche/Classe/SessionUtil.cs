﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FootilCouche.Classe
{
    public class SessionUtil
    {
        private static string nom;
        private static string prenom;
        private static string pseudo;
        private static string droit;
        private static string id;

        public static string Nom
        {
            get
            {
                return nom;
            }

            set
            {
                nom = value;
            }
        }

        public static string Prenom
        {
            get
            {
                return prenom;
            }

            set
            {
                prenom = value;
            }
        }

        public static string Pseudo
        {
            get
            {
                return pseudo;
            }

            set
            {
                pseudo = value;
            }
        }

        public static string Droit
        {
            get
            {
                return droit;
            }

            set
            {
                droit = value;
            }
        }

        public static string Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }
    }
}