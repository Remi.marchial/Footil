﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FootilCouche.Classe.DataBase
{
    public class GestionDB
    {
        MySqlConnection conn;

        public GestionDB()
        {
            connectBDD();
        }
        

        public void connectBDD()
        {
            string myConnectionString = "datasource=mysql-footilepsi.alwaysdata.net;username=137670;password=footil;database=footilepsi_bdd";
            try
            {
                conn = new MySqlConnection(myConnectionString);
                conn.Open();
            }
            catch (MySqlException ex)
            {
            }
        }

        public MySqlDataReader selectRequete(string requete)
        {
            MySqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = requete;
            MySqlDataReader reader = cmd.ExecuteReader();           
            return reader;
        }

        public void closeConnect()
        {
            conn.Close();
        }

        public void executeRequete(string requete)
        {
            MySqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = requete;
            cmd.ExecuteNonQuery();
        }

        public void creerSession(string login, string password)
        {
            string nomUtil = "";
            string prenomUtil = "";
            string pseudoUtil = "";
            string droitUtil = "";
            string idUtil = "";

            MySqlDataReader reader = selectRequete("SELECT * FROM Utilisateur WHERE login='" + login + "' AND password='" + password + "'");
            while (reader.Read())
            {
                nomUtil =  reader[1].ToString();
                prenomUtil = reader[2].ToString();
                pseudoUtil = reader[5].ToString();
                droitUtil = reader[8].ToString();
                idUtil = reader[0].ToString();
            }
            reader.Close();
            if (nomUtil.CompareTo("") != 0 && prenomUtil.CompareTo("") != 0 && pseudoUtil.CompareTo("") != 0 && droitUtil.CompareTo("") != 0)
            {
                SessionUtil.Nom = nomUtil;
                SessionUtil.Prenom = prenomUtil;
                SessionUtil.Pseudo = pseudoUtil;
                SessionUtil.Droit = droitUtil;
                SessionUtil.Id = idUtil;
            }            
        }
    }
}