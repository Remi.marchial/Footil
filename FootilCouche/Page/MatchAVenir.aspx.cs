﻿using FootilCouche.Classe.DataBase;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace FootilCouche.Page
{
    public partial class MatchAVenir : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            chargerMatchAVenir();
        }

        public void chargerMatchAVenir()
        {
            GestionDB dbManager = new GestionDB();
            MySqlDataReader reader = dbManager.selectRequete("SELECT s1.date_journee, e1.nom_equipe AS equipe_dom, e1.logo AS logo_dom, e2.nom_equipe AS equipe_ext, e2.logo AS logo_ext FROM Score AS s1 INNER JOIN Score AS s2 ON s1.id_rencontre = s2.id_rencontre INNER JOIN Equipe AS e1 ON e1.id_equipe = s1.id_equipe INNER JOIN Equipe AS e2 ON e2.id_equipe = s2.id_equipe WHERE s1.id_affrontement <> s2.id_affrontement AND s1.date_journee > NOW() GROUP BY s1.id_rencontre ORDER BY s1.date_journee");
            repeaterMatch.DataSource = reader;
            repeaterMatch.DataBind();
            reader.Close();
            dbManager.closeConnect();
        }
    }
}