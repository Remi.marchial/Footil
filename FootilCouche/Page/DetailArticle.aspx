﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Page/PageMaitresse.Master" AutoEventWireup="true" CodeBehind="DetailArticle.aspx.cs" Inherits="FootilCouche.Page.DetailArticle" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <div class="presentationAccueil">
        <ul style="padding: 0px">
            <li class="listeCom">
                <h4 id="titre" runat="server" class="catAccueil"><%# Eval("titre_article") %></h4>
                <div id="date" runat="server"><%# Eval("date_article") %></div>
                <p id="contenu" runat="server"><%# Eval("contenu_article") %></p>
                <div id="auteur" runat="server"><%# Eval("prenom_util") + " " + Eval("nom_util").ToString()[0] + "."%></div>
            </li>
            <asp:Repeater ID="rptCommentaire" runat="server" OnItemDataBound="rptCommentaire_ItemDataBound">
                <ItemTemplate>
                    <div style="margin-left: 20px;">
                        <asp:HiddenField ID="HiddenFieldId_commentaire" runat="server" value='<%# Eval("id_commentaire") %>' />
                        <asp:HiddenField ID="HiddenFieldStatut_com" runat="server" value='<%# Eval("statut_comm") %>' />
                        <h4><%# Eval("prenom_util") + " " + Eval("nom_util").ToString()[0] + ".  :"%></h4>
                        <p class="contenuCom" style="margin-bottom: 10px;"><%# Eval("contenu_commentaire")%></p>
                        <asp:Button ID="ButtonValidCom" runat="server" CssClass="buttonConnec" Text="Valider" Visible="false" OnClick="ButtonValidCom_Click"/>
                       
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </ul>
    </div>
</asp:Content>
