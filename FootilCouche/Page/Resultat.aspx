﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Resultat.aspx.cs" Inherits="FootilCouche.Page.Resultat" MasterPageFile="~/Page/PageMaitresse.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <div class="presentationAccueil">
        <ul style="padding: 0px">
            <asp:Repeater ID="repeaterMatch" runat="server">
                <ItemTemplate>
                    <li class="listeAccueil">
                        <h4 class="catAccueil"><asp:Label runat="server" Text='<%#  DateTime.Parse(Eval("date_journee").ToString()).ToString("dddd dd MMMM yyyy à HH:mm") %>' /></h4>
                        <asp:Label runat="server" ID="labelDateDernierMatch" />
                        <ul class="tabMatch">
                            <li class="colonneTabMatch">
                                <ul style="padding: 0px">
                                    <li class="tabMatchElement">
                                        <asp:Image ID="ImageEquipeDom" runat="server" ImageAlign="Middle" CssClass="imageEquipe" ImageUrl='<%#  "http://" + Eval("logo_dom") %>' /></li>
                                    <li class="tabMatchElement">
                                        <asp:Label ID="LabelEquipeDom" runat="server" Text='<%# Eval("equipe_dom") %>' /></li>
                                </ul>
                            </li>
                            <li class="colonneTabMatch">
                                <asp:Label ID="labelScoreDom" runat="server" CssClass="score" Text='<%# Eval("but_dom") %>' /></li>
                            <li class="colonneTabMatch">
                                <asp:Label runat="server" Text=" - " />
                            </li>
                            <li class="colonneTabMatch">
                                <asp:Label ID="labelScoreExt" runat="server" CssClass="score" Text='<%# Eval("but_ext") %>' /></li>
                            <li class="colonneTabMatch">
                                <ul style="padding: 0px">
                                    <li class="tabMatchElement">
                                        <asp:Image ID="ImageEquipeExt" ImageAlign="Middle" runat="server" ImageUrl='<%#  "http://" + Eval("logo_ext") %>' CssClass="imageEquipe" /></li>
                                    <li class="tabMatchElement">
                                        <asp:Label ID="LabelEquipeExt" runat="server" Text='<%# Eval("equipe_ext") %>' /></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ItemTemplate>
            </asp:Repeater>
        </ul>
    </div>
</asp:Content>
