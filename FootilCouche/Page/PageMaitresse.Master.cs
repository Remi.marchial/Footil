﻿using FootilCouche.Classe;
using FootilCouche.Classe.DataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FootilCouche.Page
{
    public partial class PageMaitresse : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            testSession();
        }

        protected void buttonInscription_Click(object sender, EventArgs e)
        {
            Response.Redirect("CreerCompte.aspx");
        }

        protected void ImageButtonLogo_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Accueil.aspx");
        }

        protected void Unnamed1_Click(object sender, EventArgs e)
        {
            Response.Redirect("Accueil.aspx");
        }

        public Boolean testSession()
        {
            Boolean test = false;
            try
            {
                if (SessionUtil.Nom != null && SessionUtil.Prenom != null && SessionUtil.Pseudo != null)
                {
                    TextBoxLogin.Visible = false;
                    TextBoxPwd.Visible = false;
                    buttonConnec.Visible = false;
                    buttonInscription.Visible = false;
                    labelMessageAccueil.Visible = true;
                    buttonDeco.Visible = true;
                    labelMessageAccueil.Text = "Bonjour " + SessionUtil.Prenom;
                    test = true;
                }
            }catch(Exception e)
            {

            }

            return test;
        }

        protected void buttonConnec_Click(object sender, EventArgs e)
        {
            GestionDB dbManager = new GestionDB();

            if (TextBoxLogin.Text.CompareTo("") != 0 && TextBoxPwd.Text.CompareTo("") != 0)
            {
                dbManager.creerSession(TextBoxLogin.Text, TextBoxPwd.Text);
                if (testSession())
                {
                    Response.Redirect("Accueil.aspx");
                }
            }
            dbManager.closeConnect();
        }

        protected void buttonDeco_Click(object sender, EventArgs e)
        {
            SessionUtil.Nom = null;
            SessionUtil.Prenom = null;
            SessionUtil.Pseudo = null;
            SessionUtil.Droit = null;
            SessionUtil.Id = null;
            Response.Redirect("Accueil.aspx");           
        }
    }
}