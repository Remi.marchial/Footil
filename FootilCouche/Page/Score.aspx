﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Page/PageMaitresse.Master" AutoEventWireup="true" CodeBehind="Score.aspx.cs" Inherits="FootilCouche.Page.Score" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <div class="presentationAccueil">
        <ul style="padding: 0px">
            <li class="listeAccueil">
                <h4 id="Titre" class="catAccueil" runat="server"></h4>

                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" CssClass="tableauScore">
                    <AlternatingRowStyle BackColor="#dcecf4" />
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                            <ItemStyle Width="100px" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="nom_equipe" HeaderText="Equipe" ReadOnly="True" />
                        <asp:BoundField DataField="points" HeaderText="Points" />
                        <asp:BoundField DataField="match_gagne" HeaderText="Matchs gagnés" />
                        <asp:BoundField DataField="match_nul" HeaderText="Matchs nuls" />
                        <asp:BoundField DataField="match_perdu" HeaderText="Matchs perdus" />
                    </Columns>
                </asp:GridView>
            </li>
        </ul>
    </div>
</asp:Content>
