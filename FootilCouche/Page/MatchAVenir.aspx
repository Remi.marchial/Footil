﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MatchAVenir.aspx.cs" Inherits="FootilCouche.Page.MatchAVenir" MasterPageFile="~/Page/PageMaitresse.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <div class="presentationAccueil">
        <ul style="padding: 0px">
            <asp:Repeater ID="repeaterMatch" runat="server">
                <ItemTemplate>
                    <li class="listeAccueil">
                        <h4 class="catAccueil"><%#  DateTime.Parse(Eval("date_journee").ToString()).ToString("dddd dd MMMM yyyy à HH:mm") %></h4>                       
                        <ul class="tabMatch">
                            <li class="colonneTabMatch">
                                <ul style="padding: 0px">
                                    <li class="tabMatchElement">
                                        <asp:Image ID="ImageEquipe1" runat="server" ImageAlign="Middle" CssClass="imageEquipe" ImageUrl='<%#  "http://" + Eval("logo_dom") %>' /></li>
                                    <li class="tabMatchElement">
                                        <asp:Label ID="labeEquipe1" runat="server" Text='<%# Eval("equipe_dom") %>' /></li>
                                </ul>
                            </li>
                            <li class="colonneTabMatch">
                                <asp:Label runat="server" Text=" - " />
                            </li>
                            <li class="colonneTabMatch">
                                <ul style="padding: 0px">
                                    <li class="tabMatchElement">
                                        <asp:Image ID="ImageEquipe2" ImageAlign="Middle" runat="server" CssClass="imageEquipe" ImageUrl='<%#  "http://" + Eval("logo_ext") %>' /></li>
                                    <li class="tabMatchElement">
                                        <asp:Label ID="labeEquipe2" runat="server" Text='<%# Eval("equipe_ext") %>' /></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ItemTemplate>
            </asp:Repeater>
        </ul>
    </div>
</asp:Content>
