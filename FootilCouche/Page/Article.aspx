﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Page/PageMaitresse.Master" AutoEventWireup="true" CodeBehind="Article.aspx.cs" Inherits="FootilCouche.Page.Article" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <div class="presentationAccueil">
        <ul style="padding: 0px">
            <asp:Repeater ID="rptArticle" runat="server">
                <ItemTemplate>
                    <li class="listeCom">
                        <asp:HiddenField ID="HiddenFieldId_article" runat="server" Value='<%# Eval("id_article") %>' />
                        <h4 class="catAccueil"><asp:HyperLink id="hyperlink1" CssClass="linkArticle" NavigateUrl='<%# String.Format("DetailArticle.aspx?id={0}", Eval("id_article"))%>' Text='<%# Eval("titre_article")%>' runat="server"/></h4>
                        <div><%# Eval("date_article") %></div>
                        <p><%# (Eval("contenu_article").ToString().Length > 50) ? (Eval("contenu_article").ToString().Substring(0, 50) + "...") : Eval("contenu_article") %></p>
                    </li>
                </ItemTemplate>
            </asp:Repeater>
        </ul>
    </div>
</asp:Content>
