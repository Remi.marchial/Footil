﻿using FootilCouche.Classe;
using FootilCouche.Classe.DataBase;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FootilCouche.Page
{
    public partial class DetailArticle : System.Web.UI.Page
    {
        string idArticle = null;
        protected string droitUtil = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            idArticle = Request.QueryString["id"];
            droitUtil = SessionUtil.Droit;

            if (!IsPostBack)
            {
                GestionDB dbManager = new GestionDB();
                MySqlDataReader reader = dbManager.selectRequete(
                    @"SELECT id_article, titre_article, contenu_article, date_article, nom_util, prenom_util FROM Article, Utilisateur
                WHERE Article.id_util=Utilisateur.id_util AND id_article=" + idArticle
                );

                reader.Read();
                titre.InnerText = reader.GetString("titre_article");
                date.InnerText = reader.GetString("date_article");
                contenu.InnerText = reader.GetString("contenu_article");
                auteur.InnerText = reader.GetString("prenom_util") + " " + reader.GetString("nom_util").ToString()[0] + ".";

                reader.Close();

                dbManager.closeConnect();

                genererCommentaire();
            }

        }

        private void genererCommentaire()
        {
            GestionDB dbManager = new GestionDB();
            MySqlDataReader reader = null;

            if (droitUtil == null || !droitUtil.Equals("2"))
            {
                reader = dbManager.selectRequete(
                    @"SELECT id_commentaire, contenu_commentaire, date_commentaire, statut_comm, nom_util, prenom_util FROM Commentaire, Utilisateur 
                    WHERE id_article=" + idArticle + " AND Commentaire.id_util=Utilisateur.id_util AND Commentaire.statut_comm=1"
                );
            }
            else
            {
                reader = dbManager.selectRequete(
                    @"SELECT id_commentaire, contenu_commentaire, date_commentaire, statut_comm, nom_util, prenom_util FROM Commentaire, Utilisateur 
                    WHERE id_article=" + idArticle + " AND Commentaire.id_util=Utilisateur.id_util"
                );
            }
            
            
            rptCommentaire.DataSource = reader;
            rptCommentaire.DataBind();
            reader.Close();
            dbManager.closeConnect();
        }

        protected void ButtonValidCom_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            RepeaterItem childItem = (RepeaterItem)btn.NamingContainer;
            
            string idCommentaire = (childItem.FindControl("HiddenFieldId_commentaire") as HiddenField).Value;
            GestionDB dbManager = new GestionDB();
            dbManager.executeRequete("UPDATE Commentaire SET statut_comm=1 WHERE id_commentaire=" + idCommentaire + " AND id_article=" + idArticle);

            Response.Redirect("DetailArticle.aspx?id=" + idArticle);
        }

        protected void rptCommentaire_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Button validateCom = (Button)(e.Item.FindControl("ButtonValidCom"));
            string statut = (e.Item.FindControl("HiddenFieldStatut_com") as HiddenField).Value;
            if (statut.Equals("0"))
            {
                validateCom.Visible = true;
            }
        }
    }
}