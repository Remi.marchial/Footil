﻿using FootilCouche.Classe;
using FootilCouche.Classe.DataBase;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FootilCouche.Page
{
    public partial class Score : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string idChampionnat = Request.QueryString["id"];

            GestionDB dbManager = new GestionDB();
            MySqlDataReader reader = dbManager.selectRequete(
                @"SELECT c1.nom_championnat, e1.nom_equipe,
                sum(CASE
                    WHEN s1.nb_but IS NULL THEN 0
                    WHEN s1.nb_but > s2.nb_but THEN 2
                    WHEN s1.nb_but < s2.nb_but THEN 0
                    ELSE 1
                END) as points,
                sum(CASE
                    WHEN s1.nb_but > s2.nb_but THEN 1
                    ELSE 0
                END) as match_gagne,
                sum(CASE
                    WHEN s1.nb_but < s2.nb_but THEN 1
                    ELSE 0
                END) as match_perdu,
                sum(CASE
                    WHEN s1.nb_but = s2.nb_but THEN 1
                    ELSE 0
                END) as match_nul
                FROM Score AS s1
                INNER JOIN Score AS s2 ON s1.id_rencontre = s2.id_rencontre
                INNER JOIN Equipe AS e1 ON e1.id_equipe = s1.id_equipe
                INNER JOIN Equipe AS e2 ON e2.id_equipe = s2.id_equipe
                INNER JOIN Championnat AS c1 ON c1.id_championnat = e1.id_championnat
                WHERE s1.id_affrontement <> s2.id_affrontement AND c1.id_championnat = " + idChampionnat + @"
                GROUP BY e1.nom_equipe DESC"
            );
            GridView1.DataSource = reader;
            GridView1.DataBind();
            Titre.InnerText = "Résultats du championnat " + reader.GetString("nom_championnat");

            reader.Close();
            dbManager.closeConnect();
        }
    }
}