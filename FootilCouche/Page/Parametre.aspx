﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Parametre.aspx.cs" Inherits="FootilCouche.Page.Parametre" MasterPageFile="~/Page/PageMaitresse.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <div class="presentationParam">

        <ul style="padding: 0px">
            <li class="listeAccueil">
                <h4 class="catAccueil">
                    <asp:Label runat="server" Text="Paramètre du compte" /></h4>
                <asp:Label ID="labelMsgConnec" runat="server" Text="Vous devez être connecté" Visible="true" />
                <ul>
                    <li class="champForm">
                        <asp:Label ID="labelLogin" runat="server" Text="Login : " Visible="false" CssClass="formulaireParam" />
                        <asp:TextBox ID="TextBoxLog" runat="server" Visible="false" />
                    </li>
                    <li class="champForm">
                        <asp:Label ID="labelPassword" runat="server" Text="Password : " Visible="false" CssClass="formulaireParam" />
                        <asp:TextBox ID="TextBoxPwd" runat="server" Visible="false" />
                    </li>

                    <li class="champForm">
                        <asp:Label ID="labelNom" runat="server" Text="Nom : " Visible="false" CssClass="formulaireParam" />
                        <asp:TextBox ID="TextBoxNom" runat="server" Visible="false" />
                    </li>
                    <li class="champForm">
                        <asp:Label ID="labelPrenom" runat="server" Text="Prénom : " Visible="false" CssClass="formulaireParam" />
                        <asp:TextBox ID="TextBoxPrenom" runat="server" Visible="false" />
                    </li>
                    <li class="champForm">
                        <asp:Label ID="labelEmail" runat="server" Text="Email : " Visible="false" CssClass="formulaireParam" />
                        <asp:TextBox ID="TextBoxEmail" runat="server" TextMode="Email" Visible="false" />
                    </li>
                    <li class="champForm">
                        <asp:Label ID="labelTel" runat="server" Text="Téléphone : " Visible="false" CssClass="formulaireParam" />
                        <asp:TextBox ID="TextBoxTel" runat="server" Visible="false" />
                    </li>
                    <li class="champForm">
                        <asp:Label ID="labelPays" runat="server" Text="Pays : " Visible="false" CssClass="formulaireParam" />
                        <asp:DropDownList ID="DropDownListPays" runat="server" Visible="false" />
                    </li>
                </ul>
                <div style="width: 50%; text-align: center;">
                    <asp:Button ID="buttonCreer" runat="server" Text="Modifier" CssClass="buttonConnec" Visible="false" OnClick="buttonCreer_Click" />
                </div>
            </li>
            <% 
                if (droitUtil != null)
                {
                    if (droitUtil.CompareTo("2") == 0)
                    {
            %>
            <li class="listeAccueil">
                <h4 class="catAccueil">
                    <asp:Label runat="server" Text="Gérer les scores" /></h4>
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" OnRowCancelingEdit="GridView1_RowCancelingEdit" OnRowUpdating="GridView1_RowUpdating" OnRowEditing="GridView1_RowEditing" CssClass="tableauScore">
                    <AlternatingRowStyle BackColor="#dcecf4" />
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:HiddenField ID="HiddenFieldId_equipe" Value='<%# Eval("id_rencontre") %>' runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:CommandField ShowEditButton="True"></asp:CommandField>
                        <asp:BoundField DataField="date_rencontre" HeaderText="Date du match" ReadOnly="True" />
                        <asp:BoundField DataField="equipe_dom" HeaderText="Equipe à domicile" ReadOnly="True" />
                        <asp:BoundField DataField="but_dom" HeaderText="Nombre de but" />
                        <asp:BoundField DataField="equipe_ext" HeaderText="Equipe en exterieur" ReadOnly="True" />
                        <asp:BoundField DataField="but_ext" HeaderText="Nombre de but" />
                    </Columns>
                    <SelectedRowStyle ForeColor="#CCFF99" Font-Bold="True"
                        BackColor="#009999"></SelectedRowStyle>
                </asp:GridView>
            </li>
            <li class="listeAccueil">
                <h4 class="catAccueil">
                    <asp:Label runat="server" Text="Gérer les équipes" /></h4>
                <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="false" CssClass="tableauScore" OnRowCancelingEdit="GridView2_RowCancelingEdit" OnRowEditing="GridView2_RowEditing" OnRowUpdating="GridView2_RowUpdating" OnRowDataBound="GridView2_RowDataBound">
                    <AlternatingRowStyle BackColor="#dcecf4" />
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:HiddenField ID="HiddenFieldId_equipe" Value='<%# Eval("id_equipe") %>' runat="server" />
                                <asp:HiddenField ID="HiddenFieldNom_stade" Value='<%# Eval("nom_stade") %>' runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:CommandField ShowEditButton="True"></asp:CommandField>
                        <asp:BoundField DataField="nom_equipe" HeaderText="Nom de l'équipe"/>
                        <asp:TemplateField HeaderText="Stade">
                            <ItemTemplate>
                                <asp:DropDownList ID="DropDownListStade" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <SelectedRowStyle ForeColor="#CCFF99" Font-Bold="True"
                        BackColor="#009999"></SelectedRowStyle>
                </asp:GridView>
                <div style="width: 100%; text-align: center;">   
                    <asp:TextBox ID="textBoxFormNomEquipe" runat="server" placeholder="Nom"/> 
                    <asp:TextBox ID="textBoxFormLogoEquipe" runat="server" placeholder="Logo"/>
                    <asp:DropDownList ID="DropDownListFormEquipeChamp" runat="server" />
                    <asp:DropDownList ID="DropDownListFormEquipePays" runat="server" /> 
                    <asp:DropDownList ID="DropDownListFormEquipeStade" runat="server" />
                    <asp:Button ID="buttonAjouterEquipe" runat="server" Text="Ajouter Equipe" OnClick="buttonAjouterEquipe_Click" CssClass="buttonFormChamp"/>
                </div>
            </li>
            <li class="listeAccueil">
                <h4 class="catAccueil">
                    <asp:Label runat="server" Text="Gérer les championnats" /></h4>
                <asp:GridView ID="GridView3" runat="server" AutoGenerateColumns="false" CssClass="tableauScore" OnRowDataBound="GridView3_RowDataBound" OnRowCancelingEdit="GridView3_RowCancelingEdit" OnRowEditing="GridView3_RowEditing" OnRowUpdating="GridView3_RowUpdating">
                    <AlternatingRowStyle BackColor="#dcecf4" />
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:HiddenField ID="HiddenFieldId_championnat" Value='<%# Eval("id_championnat") %>' runat="server" />
                                <asp:HiddenField ID="HiddenFieldNom_pays" Value='<%# Eval("nom_pays") %>' runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:CommandField ShowEditButton="True"></asp:CommandField>
                        <asp:BoundField DataField="nom_championnat" HeaderText="Nom du championnat"/>
                        <asp:TemplateField HeaderText="Pays">
                            <ItemTemplate>
                                <asp:DropDownList ID="DropDownListPaysChamp" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <SelectedRowStyle ForeColor="#CCFF99" Font-Bold="True"
                        BackColor="#009999"></SelectedRowStyle>
                </asp:GridView>
                <div style="width: 100%; text-align: center;">
                    <asp:TextBox id="textBoxChamp" runat="server" placeholder="Nom du championnat" />
                    <asp:TextBox id="textBoxImageChamp" runat="server" placeholder="Url Image" />
                    <asp:DropDownList id="dropDownListFormChamp" runat="server"/>
                    <asp:Button id="buttonAjouterChampionnat" runat="server" Text="Ajouter Championnat" CssClass="buttonFormChamp" OnClick="buttonAjouterChampionnat_Click" />
                </div>
            </li>
            <li class="listeAccueil">
                <h4 class="catAccueil">
                    <asp:Label runat="server" Text="Gérer les stades" /></h4>
                <asp:GridView ID="GridView4" runat="server" AutoGenerateColumns="false" CssClass="tableauScore" OnRowCancelingEdit="GridView4_RowCancelingEdit" OnRowEditing="GridView4_RowEditing" OnRowUpdating="GridView4_RowUpdating" >
                    <AlternatingRowStyle BackColor="#dcecf4" />
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:HiddenField ID="HiddenFieldId_stade" Value='<%# Eval("id_stade") %>' runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:CommandField ShowEditButton="True"></asp:CommandField>
                        <asp:BoundField DataField="nom_stade" HeaderText="Nom"/>
                        <asp:BoundField DataField="capacite" HeaderText="Capacite"/>
                        <asp:BoundField DataField="adresse_stade" HeaderText="Adresse"/>
                    </Columns>
                    <SelectedRowStyle ForeColor="#CCFF99" Font-Bold="True"
                        BackColor="#009999"></SelectedRowStyle>
                </asp:GridView>
                <div style="width: 100%; text-align: center;">
                    <asp:TextBox id="TextBoxNomStade" runat="server" placeholder="Nom" />
                    <asp:TextBox id="TextBoxCapaciteStade" runat="server" placeholder="capacite" />
                    <asp:TextBox id="TextBoxAdresseStade" runat="server" placeholder="adresse" />
                    <asp:Button ID="buttonAjouterStade" runat="server" Text="Ajouter Stade" CssClass="buttonFormChamp" OnClick="buttonAjouterStade_Click"/>
                </div>
            </li>

            <% }
                }%>
        </ul>
    </div>
</asp:Content>
