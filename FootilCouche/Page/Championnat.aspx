﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Page/PageMaitresse.Master" AutoEventWireup="true" CodeBehind="Championnat.aspx.cs" Inherits="FootilCouche.Page.Championnat" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <div class="contenu">
        
        <asp:DataList ID="DataList1" runat="server" RepeatColumns="3" 
            RepeatDirection="Horizontal" RepeatLayout="Flow">
            <ItemTemplate >
                <div style="display: inline-block; margin:20px; font-family:'Arial Rounded MT';">
                    <ul style="padding:0px">
                        <li class="tabMatchElement">
                            <asp:ImageButton ID="Image1" runat="server" CssClass="imageChamp" ImageAlign="Middle" ImageUrl='<%# "http://" + Eval("logo_championnat") %>' PostBackUrl='<%# String.Format("Score.aspx?id={0}", Eval("id_championnat"))%>'/>
                        </li>
                        <li class="tabMatchElement">
                            <asp:HyperLink id="hyperlink1" NavigateUrl='<%# String.Format("Score.aspx?id={0}", Eval("id_championnat"))%>' Text='<%# Eval("nom_championnat")%>' runat="server"/>
                        </li>
                    </ul>
                </div>
               
            </ItemTemplate>            
        </asp:DataList>

    </div>

</asp:Content>
