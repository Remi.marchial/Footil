﻿using FootilCouche.Classe.DataBase;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FootilCouche.Page
{
    public partial class Article : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GestionDB dbManager = new GestionDB();
                MySqlDataReader reader = dbManager.selectRequete(
                    @"SELECT id_article, titre_article, contenu_article, date_article, nom_util, prenom_util FROM Article, Utilisateur
                WHERE Article.id_util=Utilisateur.id_util"
                );
                rptArticle.DataSource = reader;
                rptArticle.DataBind();
                reader.Close();

                dbManager.closeConnect();
            }

            
        }

        /*otected void rptArticle_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater childRepeater = (Repeater)e.Item.FindControl("rptCommentaire");
                string idArticle = (e.Item.FindControl("HiddenFieldId_article") as HiddenField).Value;
                
                GestionDB dbManager = new GestionDB();
                MySqlDataReader reader = dbManager.selectRequete(
                    @"SELECT id_commentaire, contenu_commentaire, date_commentaire, statut_comm, nom_util, prenom_util FROM Commentaire, Utilisateur 
                    WHERE id_article=" + idArticle + " AND Commentaire.id_util=Utilisateur.id_util"
                );
                childRepeater.DataSource = reader;
                childRepeater.DataBind();
                reader.Close();
                dbManager.closeConnect();
            }
        }*/

        protected void ButtonValidCom_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            RepeaterItem childItem = (RepeaterItem)btn.NamingContainer;
            Repeater parentRepeat = (Repeater)childItem.NamingContainer;

            /*ControlCollection ctrlColl = repeaterParent.Controls; foreach (Control ctrl in ctrlColl)

            {

                if (ctrl is RepeaterItem)

                {
                    lblCatDescr = (Label)ctrl.FindControl("");

                }

            }*/


            //string idArticle = (test.FindControl("HiddenFieldId_article") as HiddenField).Value;
            GestionDB dbManager = new GestionDB();

            //dbManager.executeRequete("UPDATE Commentaire SET statut_comm=1 WHERE id_article=" + idArticle);
        }
    }
}