﻿using FootilCouche.Classe.DataBase;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FootilCouche.Page
{
    public partial class Accueil : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            chargerArticle();
            chargerMatchAVenir();
            chargerDernierMatch();
        }

        public void chargerArticle()
        {
            GestionDB dbManager = new GestionDB();
            MySqlDataReader reader = dbManager.selectRequete("SELECT  `titre_article` ,  `contenu_article` FROM  `Article` WHERE  `date_article` < NOW() ORDER BY  `date_article` DESC LIMIT 1");
            while (reader.Read())
            {
                labelTitreArticle.Text = reader[0].ToString();
                labelContenuArticle.Text = reader[1].ToString();
            }
            reader.Close();
            dbManager.closeConnect();
        }

        public void chargerMatchAVenir()
        {
            string idEquipeDomicile = "";
            string idEquipeExterieur = "";
            GestionDB dbManager = new GestionDB();

            //id equipe domicile
            MySqlDataReader reader = dbManager.selectRequete("SELECT  `id_equipe` , `date_journee` FROM  `Score` WHERE  `statut_equip` = 1 AND  `date_journee` > NOW() ORDER BY  `date_journee` ASC LIMIT 1");
            while (reader.Read())
            {
                idEquipeDomicile = reader[0].ToString();
                labelDateMatchAVenir.Text = reader[1].ToString();
            }
            reader.Close();

            //id equipe exterieur
            reader = dbManager.selectRequete("SELECT  `id_equipe`FROM  `Score` WHERE  `statut_equip` = 2 AND  `date_journee` > NOW() ORDER BY  `date_journee` ASC LIMIT 1");
            while (reader.Read())
            {
                idEquipeExterieur = reader[0].ToString();
            }
            reader.Close();

            //nom equipe domicile
            reader = dbManager.selectRequete("SELECT  `nom_equipe` ,  `logo` FROM  `Equipe` WHERE  `id_equipe` = " + idEquipeDomicile);
            while (reader.Read())
            {
                labeEquipe1.Text = reader[0].ToString();
                ImageEquipe1.ImageUrl = "http://" + reader[1].ToString();
            }
            reader.Close();

            //nom equipe exterieur + date match 
            reader = dbManager.selectRequete("SELECT  `nom_equipe` ,  `logo` FROM  `Equipe` WHERE  `id_equipe` = " + idEquipeExterieur);
            while (reader.Read())
            {
                labeEquipe2.Text = reader[0].ToString();
                ImageEquipe2.ImageUrl = "http://" + reader[1].ToString();
            }

            reader.Close();
            dbManager.closeConnect();
        }

        public void chargerDernierMatch()
        {
            
            //
            string idEquipeDomicile = "";
            string idEquipeExterieur = "";
            GestionDB dbManager = new GestionDB();

            //id equipe domicile
            MySqlDataReader reader = dbManager.selectRequete("SELECT  `id_equipe` , `date_journee` , `nb_but` FROM  `Score` WHERE  `statut_equip` = 1 AND  `date_journee` < NOW() ORDER BY  `date_journee` DESC LIMIT 1");
            while (reader.Read())
            {
                idEquipeDomicile = reader[0].ToString();
                labelDateMatchAVenir.Text = reader[1].ToString();
                labelScoreDom.Text = reader[2].ToString();
            }
            reader.Close();

            //id equipe exterieur
            reader = dbManager.selectRequete("SELECT  `id_equipe`, `nb_but` FROM  `Score` WHERE  `statut_equip` = 2 AND  `date_journee` < NOW() ORDER BY  `date_journee` DESC LIMIT 1");
            while (reader.Read())
            {
                idEquipeExterieur = reader[0].ToString();
                labelScoreExt.Text = reader[1].ToString();
            }
            reader.Close();

            //nom equipe domicile
            reader = dbManager.selectRequete("SELECT  `nom_equipe` ,  `logo` FROM  `Equipe` WHERE  `id_equipe` = " + idEquipeDomicile);
            while (reader.Read())
            {
                LabelEquipeDom.Text = reader[0].ToString();
                ImageEquipeDom.ImageUrl = "http://" + reader[1].ToString();
            }
            reader.Close();

            //nom equipe exterieur + date match 
            reader = dbManager.selectRequete("SELECT  `nom_equipe` ,  `logo` FROM  `Equipe` WHERE  `id_equipe` = " + idEquipeExterieur);
            while (reader.Read())
            {
                LabelEquipeExt.Text = reader[0].ToString();
                ImageEquipeExt.ImageUrl = "http://" + reader[1].ToString();
            }

            reader.Close();
            dbManager.closeConnect();
        }
    }    
}