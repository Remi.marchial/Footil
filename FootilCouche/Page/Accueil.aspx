﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Accueil.aspx.cs" Inherits="FootilCouche.Page.Accueil" MasterPageFile="~/Page/PageMaitresse.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <div class="presentationAccueil">
        <ul style="padding:0px">
            <li class="listeAccueil">
                <h4 class="catAccueil"><asp:Label runat="server" Text="Dernier Article" /></h4>
                <asp:Label runat="server" Text="Zizou le retour" CssClass="titreAccueil" ID="labelTitreArticle" />
                <p style="font-size:14px;margin-left:4px;">
                    <asp:Label runat="server" Text=" le contenu de ce article ma fois fort plaisant à lire !!!" id="labelContenuArticle"/>
                </p>
            </li>
            <li class="listeAccueil">
                <h4 class="catAccueil"><asp:Label runat="server" Text="Dernier Match" /></h4>
                <asp:Label runat="server" ID="labelDateDernierMatch"/>
                <ul class="tabMatch">
                    <li class="colonneTabMatch">
                        <ul style="padding:0px">
                            <li class="tabMatchElement"><asp:Image ID="ImageEquipeDom" runat="server" ImageAlign="Middle" CssClass="imageEquipe" ImageUrl="~/ImageSite/Webp.net-resizeimage.png" /></li>
                            <li class="tabMatchElement"><asp:Label id="LabelEquipeDom" runat="server" Text="Lyon"/></li>
                          </ul>
                    </li>
                    <li class="colonneTabMatch"><asp:Label ID="labelScoreDom" runat="server" CssClass="score"/></li>
                    <li class="colonneTabMatch">
                        <asp:Label runat="server" Text=" - "/>
                    </li>
                    <li class="colonneTabMatch"><asp:Label ID="labelScoreExt" runat="server" CssClass="score"/></li>
                    <li class="colonneTabMatch">                        
                        <ul style="padding:0px">
                            <li class="tabMatchElement"><asp:Image ID="ImageEquipeExt" ImageAlign="Middle" runat="server" ImageUrl="~/ImageSite/Webp.net-resizeimage.png" CssClass="imageEquipe" /></li>
                            <li class="tabMatchElement"><asp:Label id="LabelEquipeExt" runat="server" Text="Paris"/></li>
                        </ul>
                    </li>                 
                </ul>
            </li>
            <li class="listeAccueil">
                <h4 class="catAccueil"><asp:Label runat="server" Text="Match à venir" CssClass="titreAccueil" /></h4>
                <asp:Label runat="server" ID="labelDateMatchAVenir"/>
                <ul class="tabMatch">
                    <li class="colonneTabMatch">
                        <ul style="padding:0px">
                            <li class="tabMatchElement"><asp:Image ID="ImageEquipe1" runat="server" ImageAlign="Middle" CssClass="imageEquipe" ImageUrl="~/ImageSite/Webp.net-resizeimage.png" /></li>                   
                            <li class="tabMatchElement"><asp:Label id="labeEquipe1" runat="server" Text="Lyon"/></li>
                          </ul>
                    </li>
                    <li class="colonneTabMatch">
                        <asp:Label runat="server" Text=" - "/>
                    </li>
                    <li class="colonneTabMatch">                        
                        <ul style="padding:0px">
                            <li class="tabMatchElement"><asp:Image ID="ImageEquipe2" ImageAlign="Middle" runat="server" CssClass="imageEquipe" ImageUrl="~/ImageSite/Webp.net-resizeimage.png" /></li>
                            <li class="tabMatchElement"><asp:Label id="labeEquipe2" runat="server" Text="Paris"/></li>
                        </ul>
                    </li>                 
                </ul>
            </li>
        </ul>
    </div>
</asp:Content>
