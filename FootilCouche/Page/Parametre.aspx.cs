﻿using FootilCouche.Classe;
using FootilCouche.Classe.DataBase;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FootilCouche.Page
{
    public partial class Parametre : System.Web.UI.Page
    {
        protected String droitUtil = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            TextBoxPwd.Attributes["type"] = "password";
            droitUtil = SessionUtil.Droit;

            if (!IsPostBack)
            {
                if (testConnec())
                {
                    affichageFormulaireCompte();
                    gererScore();
                    gererEquipe();
                    gererStade();
                    gererChampionnat();
                    formulaireChampionnat();
                    formulaireEquipe();
                }
            }
        }

        public Boolean testConnec()
        {
            Boolean test = false;

            if (SessionUtil.Nom != null)
            {
                test = true;
            }

            return test;
        }

        //Formulaire compte
        public void affichageFormulaireCompte()
        {
            labelMsgConnec.Visible = false;
            labelLogin.Visible = true;
            labelPassword.Visible = true;
            labelNom.Visible = true;
            labelPrenom.Visible = true;
            labelTel.Visible = true;
            labelEmail.Visible = true;
            labelPays.Visible = true;
            buttonCreer.Visible = true;
            TextBoxLog.Visible = true;
            TextBoxPwd.Visible = true;
            TextBoxNom.Visible = true;
            TextBoxPrenom.Visible = true;
            TextBoxTel.Visible = true;
            TextBoxEmail.Visible = true;
            DropDownListPays.Visible = true;

            //formulaire
            int index = 0;
            GestionDB dbManager = new GestionDB();

            MySqlDataReader reader = dbManager.selectRequete("SELECT `login`, `password`, `nom_util`, `prenom_util`, `email`, `tel_util`, `id_pays`, `id_util` FROM `Utilisateur` WHERE `login`='" + SessionUtil.Pseudo + "'");
            while (reader.Read())
            {
                TextBoxLog.Text = reader[0].ToString();
                TextBoxPwd.Text = reader[1].ToString();
                TextBoxNom.Text = reader[2].ToString();
                TextBoxPrenom.Text = reader[3].ToString();
                TextBoxEmail.Text = reader[4].ToString();
                TextBoxTel.Text = reader[5].ToString();
                index = reader.GetInt32("id_pays");
            }
            reader.Close();

            //liste des pays
            reader = dbManager.selectRequete("SELECT * FROM Pays");
            DropDownListPays.DataSource = reader;
            DropDownListPays.DataValueField = "id_pays";
            DropDownListPays.DataTextField = "nom_pays";
            DropDownListPays.DataBind();
            DropDownListPays.SelectedValue = index.ToString();

            reader.Close();
            dbManager.closeConnect();
        }

        protected void buttonCreer_Click(object sender, EventArgs e)
        {
            GestionDB dbManager = new GestionDB();

            if (TextBoxLog.Text.CompareTo("") == 0)
            {
                TextBoxLog.BackColor = System.Drawing.Color.Red;
            }
            else if (TextBoxPwd.Text.CompareTo("") == 0)
            {
                TextBoxPwd.BackColor = System.Drawing.Color.Red;
            }
            else if (TextBoxNom.Text.CompareTo("") == 0)
            {
                TextBoxNom.BackColor = System.Drawing.Color.Red;
            }
            else if (TextBoxPrenom.Text.CompareTo("") == 0)
            {
                TextBoxPrenom.BackColor = System.Drawing.Color.Red;
            }
            else if (TextBoxEmail.Text.CompareTo("") == 0)
            {
                TextBoxEmail.BackColor = System.Drawing.Color.Red;
            }
            else if (TextBoxTel.Text.CompareTo("") == 0)
            {
                TextBoxTel.BackColor = System.Drawing.Color.Red;
            }
            else
            {
                SessionUtil.Pseudo = TextBoxLog.Text;
                SessionUtil.Nom = TextBoxNom.Text;
                SessionUtil.Prenom = TextBoxPrenom.Text;
                dbManager.executeRequete("UPDATE Utilisateur SET login =  '" + TextBoxLog.Text + "', PASSWORD = '" + TextBoxPwd.Text + "', prenom_util = '" + TextBoxPrenom.Text + "', nom_util = '" + TextBoxNom.Text + "', email = '" + TextBoxEmail.Text + "', tel_util = '" + TextBoxTel.Text + "', id_pays =" + DropDownListPays.SelectedValue + " WHERE id_util =" + SessionUtil.Id);
                Response.Redirect("Accueil.aspx");

            }
            dbManager.closeConnect();
        }

        //Onglet score
        public void gererScore()
        {
            GestionDB dbManager = new GestionDB();
            MySqlDataReader reader = dbManager.selectRequete(
            @"SELECT s1.date_journee AS date_rencontre, s1.id_rencontre, e1.nom_equipe AS equipe_dom, s1.nb_but AS but_dom ,e2.nom_equipe AS equipe_ext, s2.nb_but AS but_ext FROM Score AS s1
            	INNER JOIN Score AS s2 ON s1.id_rencontre = s2.id_rencontre
            	INNER JOIN Equipe AS e1 ON e1.id_equipe = s1.id_equipe
            	INNER JOIN Equipe AS e2 ON e2.id_equipe = s2.id_equipe
   	     	    WHERE s1.id_affrontement <> s2.id_affrontement AND s1.statut_equip = 1 AND s1.date_journee < NOW() AND s1.date_journee > (NOW() - INTERVAL 1 WEEK)
            	GROUP BY s1.id_rencontre ORDER BY s1.date_journee DESC");
            GridView1.DataSource = reader;
            GridView1.DataBind();
            reader.Close();
            dbManager.closeConnect();
        }

        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
        }

        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GestionDB dbManager = new GestionDB();

            GridViewRow row = GridView1.Rows[e.RowIndex];
            HiddenField theHiddenField = row.FindControl("HiddenFieldId_rencontre") as HiddenField;
            string but_dom = ((TextBox)(row.Cells[4].Controls[0])).Text;
            string but_ext = ((TextBox)(row.Cells[6].Controls[0])).Text;
            string id_rencontre = theHiddenField.Value;
            dbManager.executeRequete("UPDATE Score set nb_but='" + but_dom + "' WHERE id_rencontre =" + id_rencontre + " AND statut_equip=1");
            dbManager.executeRequete("UPDATE Score set nb_but='" + but_ext + "' WHERE id_rencontre =" + id_rencontre + " AND statut_equip=2");
            dbManager.closeConnect();
            GridView1.EditIndex = -1;
            gererScore();
        }

        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1;
        }

        //Onglet Equipe
        public void gererEquipe()
        {
            GestionDB dbManager = new GestionDB();
            MySqlDataReader reader = dbManager.selectRequete(@"SELECT id_equipe, nom_equipe , Stade.id_stade,Stade.nom_stade FROM Equipe, Stade WHERE Stade.id_stade=Equipe.id_stade");
            GridView2.DataSource = reader;
            GridView2.DataBind();
            reader.Close();
            dbManager.closeConnect();
        }

        protected void GridView2_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GestionDB dbManager = new GestionDB();

            GridViewRow row = GridView2.Rows[e.RowIndex];
            HiddenField theHiddenField = row.FindControl("HiddenFieldId_equipe") as HiddenField;
            string nom_equipe = ((TextBox)(row.Cells[2].Controls[0])).Text;
            string id_stade = ((DropDownList)(row.Cells[3].Controls[1])).SelectedValue;
            string id_equipe = theHiddenField.Value;

            dbManager.executeRequete("UPDATE Equipe SET nom_equipe='" + nom_equipe + "', id_stade="+ id_stade +" WHERE id_equipe =" + id_equipe);
            dbManager.closeConnect();
            GridView2.EditIndex = -1;
            gererEquipe();
        }

        protected void GridView2_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView2.EditIndex = e.NewEditIndex;
        }

        protected void GridView2_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView2.EditIndex = -1;
        }

        protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                GestionDB dbManager = new GestionDB();

                DropDownList ddlStade = (e.Row.FindControl("DropDownListStade") as DropDownList);
                MySqlDataReader reader = dbManager.selectRequete("SELECT DISTINCT * FROM Stade");
                ddlStade.DataSource = reader;
                ddlStade.DataTextField = "nom_stade";
                ddlStade.DataValueField = "id_stade";
                ddlStade.DataBind();

                string stade = (e.Row.FindControl("HiddenFieldNom_stade") as HiddenField).Value;
                ddlStade.Items.FindByText(stade).Selected = true;

                reader.Close();
                dbManager.closeConnect();
            }
        }

        public void formulaireEquipe()
        {
            GestionDB dbManager = new GestionDB();

            //liste des stades
            MySqlDataReader reader = dbManager.selectRequete("SELECT * FROM Stade");
            DropDownListFormEquipeStade.DataSource = reader;
            DropDownListFormEquipeStade.DataValueField = "id_stade";
            DropDownListFormEquipeStade.DataTextField = "nom_stade";
            DropDownListFormEquipeStade.DataBind();
            reader.Close();

            //liste des championnats
            reader = dbManager.selectRequete("SELECT * FROM Championnat");
            DropDownListFormEquipeChamp.DataSource = reader;
            DropDownListFormEquipeChamp.DataValueField = "id_championnat";
            DropDownListFormEquipeChamp.DataTextField = "nom_championnat";
            DropDownListFormEquipeChamp.DataBind();
            reader.Close();

            //liste des Pays
            reader = dbManager.selectRequete("SELECT * FROM Pays");
            DropDownListFormEquipePays.DataSource = reader;
            DropDownListFormEquipePays.DataValueField = "id_pays";
            DropDownListFormEquipePays.DataTextField = "nom_pays";
            DropDownListFormEquipePays.DataBind();
            reader.Close();

            dbManager.closeConnect();
        }

        protected void buttonAjouterEquipe_Click(object sender, EventArgs e)
        {
            GestionDB dbManager = new GestionDB();

            if (textBoxFormNomEquipe.Text.CompareTo("") != 0 && textBoxFormLogoEquipe.Text.CompareTo("") != 0)
            {
                dbManager.executeRequete("INSERT INTO  `footilepsi_bdd`.`Equipe` (`id_equipe`, `nom_equipe`, `logo`,`id_pays`,`id_championnat`,`id_stade`) VALUES(NULL, '" + textBoxFormNomEquipe.Text + "', '" + textBoxFormLogoEquipe.Text + "', '" + DropDownListFormEquipePays.SelectedValue + "', '"+ DropDownListFormEquipeChamp.SelectedValue +"','"+ DropDownListFormEquipeStade.SelectedValue + "');");
                Response.Redirect("Parametre.aspx");
            }
            dbManager.closeConnect();
        }


        //Onglet championnat
        public void gererChampionnat()
        {
            GestionDB dbManager = new GestionDB();
            MySqlDataReader reader = dbManager.selectRequete(
            @"SELECT id_championnat,nom_championnat, Pays.nom_pays FROM Championnat,Pays WHERE Championnat.id_pays=Pays.id_pays");
            GridView3.DataSource = reader;
            GridView3.DataBind();
            reader.Close();
            dbManager.closeConnect();
        }

        protected void GridView3_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                GestionDB dbManager = new GestionDB();

                DropDownList ddlPays = (e.Row.FindControl("DropDownListPaysChamp") as DropDownList);
                MySqlDataReader reader = dbManager.selectRequete("SELECT DISTINCT * FROM Pays");
                ddlPays.DataSource = reader;
                ddlPays.DataTextField = "nom_pays";
                ddlPays.DataValueField = "id_pays";
                ddlPays.DataBind();

                string pays = (e.Row.FindControl("HiddenFieldNom_pays") as HiddenField).Value;
                ddlPays.Items.FindByText(pays).Selected = true;

                reader.Close();
                dbManager.closeConnect();
            }
        }

        protected void GridView3_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GestionDB dbManager = new GestionDB();

            GridViewRow row = GridView3.Rows[e.RowIndex];
            HiddenField theHiddenField = row.FindControl("HiddenFieldId_championnat") as HiddenField;
            string nom_champ = ((TextBox)(row.Cells[2].Controls[0])).Text;
            string id_pays = ((DropDownList)(row.Cells[3].Controls[1])).SelectedValue;
            string id_champ = theHiddenField.Value;
            dbManager.executeRequete("UPDATE Championnat SET nom_championnat='" + nom_champ + "', id_pays='" + id_pays + "' WHERE id_championnat=" + id_champ);
            dbManager.closeConnect();
            GridView3.EditIndex = -1;
            gererChampionnat();
        }

        protected void GridView3_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView3.EditIndex = e.NewEditIndex;
        }

        protected void GridView3_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView3.EditIndex = -1;
        }

        protected void buttonAjouterChampionnat_Click(object sender, EventArgs e)
        {
            GestionDB dbManager = new GestionDB();

            if (textBoxChamp.Text.CompareTo("") != 0 && textBoxImageChamp.Text.CompareTo("") != 0)
            {
                dbManager.executeRequete("INSERT INTO  `footilepsi_bdd`.`Championnat` (`id_championnat`, `nom_championnat`, `logo_championnat`,`id_pays`) VALUES(NULL, '" + textBoxChamp.Text + "', '" + textBoxImageChamp.Text + "', '" + dropDownListFormChamp.SelectedValue + "');");
                Response.Redirect("Parametre.aspx");
            }
            dbManager.closeConnect();
        }

        public void formulaireChampionnat()
        {
            GestionDB dbManager = new GestionDB();
            //liste des pays
            MySqlDataReader reader = dbManager.selectRequete("SELECT * FROM Pays");
            dropDownListFormChamp.DataSource = reader;
            dropDownListFormChamp.DataValueField = "id_pays";
            dropDownListFormChamp.DataTextField = "nom_pays";
            dropDownListFormChamp.DataBind();

            reader.Close();
            dbManager.closeConnect();
        }

        //Onglet stade
        public void gererStade()
        {
            GestionDB dbManager = new GestionDB();
            MySqlDataReader reader = dbManager.selectRequete(
            @"SELECT id_stade, nom_stade,  capacite, adresse_stade FROM Stade");
            GridView4.DataSource = reader;
            GridView4.DataBind();
            reader.Close();
            dbManager.closeConnect();
        }

        protected void GridView4_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GestionDB dbManager = new GestionDB();

            GridViewRow row = GridView4.Rows[e.RowIndex];
            HiddenField theHiddenField = row.FindControl("HiddenFieldId_stade") as HiddenField;
            string nom_stade = ((TextBox)(row.Cells[2].Controls[0])).Text;
            string capacite = ((TextBox)(row.Cells[3].Controls[0])).Text;
            string adresse = ((TextBox)(row.Cells[4].Controls[0])).Text;
            string id_stade = theHiddenField.Value;
            dbManager.executeRequete("UPDATE Stade SET nom_stade='" + nom_stade + "', capacite='" + capacite + "', adresse_stade='" + adresse + "' WHERE id_stade=" + id_stade);
            dbManager.closeConnect();
            GridView4.EditIndex = -1;
            gererStade();
        }

        protected void GridView4_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView4.EditIndex = e.NewEditIndex;
        }

        protected void GridView4_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView4.EditIndex = -1;
        }

        protected void buttonAjouterStade_Click(object sender, EventArgs e)
        {
            GestionDB dbManager = new GestionDB();

            if (TextBoxNomStade.Text.CompareTo("") != 0 && TextBoxCapaciteStade.Text.CompareTo("") != 0 && TextBoxAdresseStade.Text.CompareTo("") != 0)
            {
                dbManager.executeRequete("INSERT INTO  `footilepsi_bdd`.`Stade` (`id_stade`, `nom_stade`, `capacite`, `adresse_stade`) VALUES(NULL, '" + TextBoxNomStade.Text + "', '" + TextBoxCapaciteStade.Text + "', '" + TextBoxAdresseStade.Text + "');");
                Response.Redirect("Parametre.aspx");
            }
            dbManager.closeConnect();
        }       
    }
}