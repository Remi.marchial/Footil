﻿using FootilCouche.Classe.DataBase;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FootilCouche.Page
{
    public partial class CreerCompte : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            alimenterListePays();                               
        }

        protected void ButtonCreer_Click(object sender, EventArgs e)
        {
            GestionDB dbManager = new GestionDB();

            if (TextBoxLogin.Text.CompareTo("") == 0)
            {
                TextBoxLogin.BackColor = System.Drawing.Color.Red;
            }
            else if (TextBoxPassword.Text.CompareTo("") == 0)
            {
                TextBoxPassword.BackColor = System.Drawing.Color.Red;
            }
            else if (TextBoxNom.Text.CompareTo("") == 0)
            {
                TextBoxNom.BackColor = System.Drawing.Color.Red;
            }
            else if (TextBoxPrenom.Text.CompareTo("") == 0)
            {
                TextBoxPrenom.BackColor = System.Drawing.Color.Red;
            }
            else if (TextBoxEmail.Text.CompareTo("") == 0)
            {
                TextBoxEmail.BackColor = System.Drawing.Color.Red;
            }
            else if (TextBoxTel.Text.CompareTo("") == 0)
            {
                TextBoxTel.BackColor = System.Drawing.Color.Red;
            }
            else
            {
                dbManager.executeRequete("Insert INTO Utilisateur (id_util, nom_util, prenom_util, email, tel_util, login, password, id_pays, id_droit) VALUES (null, '" + TextBoxNom.Text + "', '" + TextBoxPrenom.Text + "', '" + TextBoxEmail.Text + "', '" + TextBoxTel.Text + "', '" + TextBoxLogin.Text + "', '" + TextBoxPassword.Text + "'," + DropDownListPays.SelectedValue + ", 1);");
                dbManager.creerSession(TextBoxLogin.Text, TextBoxPassword.Text);
                Response.Redirect("Accueil.aspx");                
           }
            dbManager.closeConnect();               
        }

        public void alimenterListePays()
        {           
            GestionDB dbManager = new GestionDB();
            MySqlDataReader reader = dbManager.selectRequete("SELECT * FROM Pays");
            DropDownListPays.DataSource = reader;
            DropDownListPays.DataValueField = "id_pays";
            DropDownListPays.DataTextField = "nom_pays";
            DropDownListPays.DataBind();
            reader.Close();
            dbManager.closeConnect();
        }
    }
}