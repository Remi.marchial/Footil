﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreerCompte.aspx.cs" Inherits="FootilCouche.Page.CreerCompte" MasterPageFile="~/Page/PageMaitresse.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <div class="contenu">
        <ul class="donnees">
            <li><asp:TextBox ID="TextBoxLogin" placeholder="Login" runat="server"/></li>
            <li><asp:TextBox ID="TextBoxPassword" placeholder="Mot de passe" TextMode="Password" runat="server" /></li>
            <li><asp:TextBox ID="TextBoxNom" placeholder="Nom" runat="server" /></li>
            <li><asp:TextBox ID="TextBoxPrenom" placeholder="Prénom" runat="server" /></li>
            <li><asp:TextBox ID="TextBoxEmail" placeholder="Email" TextMode="Email" runat="server" /></li>
            <li><asp:TextBox ID="TextBoxTel" placeholder="Numéro de téléphone" runat="server" /></li>
            <li><asp:DropDownList ID="DropDownListPays" runat="server" /></li>
            <li><asp:Button ID="ButtonCreer" runat="server" Text="Creer Compte" CssClass="buttonConnec" OnClick="ButtonCreer_Click"/></li>
         </ul>
    </div>
</asp:Content>
