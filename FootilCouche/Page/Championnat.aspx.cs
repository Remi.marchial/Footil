﻿using FootilCouche.Classe.DataBase;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FootilCouche.Page
{
    public partial class Championnat : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GestionDB dbManager = new GestionDB();
            MySqlDataReader reader = dbManager.selectRequete("SELECT * FROM Championnat");
            DataList1.DataSource = reader;
            DataList1.DataBind();
            reader.Close();
            dbManager.closeConnect();
        }
    }
}